//
//  AppDelegate.swift
//  GelCAP
//
//  Created by Pritesh Pethani on 14/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit

extension String {
    var stringByDeletingLastPathComponent: String {
        
        get {
            
            return (self as NSString).stringByDeletingLastPathComponent
        }  
    }
    
}

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var bundle:NSBundle!
    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.setLanguage("")
        self.setupSetting()
        
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
        return .All;

    }
    
    func setupSetting(){
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .None)
        
        if USERDEFAULT.valueForKey("jpeg") == nil &&  USERDEFAULT.valueForKey("png") == nil && USERDEFAULT.valueForKey("tiff") == nil{
            USERDEFAULT.setValue("JPEG", forKey: "jpeg")
        }
        
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.Dark)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Black)
    }
    
    func setLanguage(languageCode:String){
        
        //    English - en
        //    chinese traditional language code :  zh-Hant
        //    japanese language code - ja
        
//        let languageCode = "ja"
//        let bundlePath = NSBundle.mainBundle().pathForResource("Localizable", ofType: "strings", inDirectory: nil, forLocalization: languageCode)!
//        APPDELEGATE.bundle = NSBundle.init(path: bundlePath.stringByDeletingLastPathComponent)
        
        
        APPDELEGATE.bundle = NSBundle.mainBundle()
//        NSLocalizedString("login", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
      
        print(NSLocalizedString("login", comment: ""))
        
        print(NSLocalizedString("Piyush", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
}

