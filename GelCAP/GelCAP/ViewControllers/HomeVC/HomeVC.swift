//
//  HomeVC.swift
//  GelCAP
//
//  Created by Pritesh Pethani on 14/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit
import ImageIO
import MobileCoreServices

import Photos

import GLKit
import AVFoundation

typealias CompletionHandler = (success:Bool!) -> Void

private let rotationTransform = CGAffineTransformMakeRotation(CGFloat(0.0))//(CGFloat(-M_PI * 0.5))

@available(iOS 8.0, *)
class HomeVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {

    //MARK: - VARIABLE DECLARATION
    @IBOutlet var CamaraView:UIView!
    

    @IBOutlet var photoView:UIImageView!
    @IBOutlet var mySlider:UISlider!
    var actionSheet:UIActionSheet!
    var imagePicker:UIImagePickerController!
    
    var videoDevice:AVCaptureDevice?
    var _previewLayer:AVCaptureVideoPreviewLayer?
    var _captureSession:AVCaptureSession?
    var stillImageOutput:AVCaptureStillImageOutput?
    
    //Creating Album in Gallery
    var assetCollection: PHAssetCollection!
    var albumFound : Bool = false
    var photosAsset: PHFetchResult!
    var assetThumbnailSize:CGSize!
    var collection: PHAssetCollection!
    var assetCollectionPlaceholder: PHObjectPlaceholder!

    //Gray Scale Custom Camera Filter
    private var context: CIContext!
    private var targetRect: CGRect!
    private var session: AVCaptureSession!
    private var filter: CIFilter!
    var isCaptureImage: Bool = false
    
    @IBOutlet var glView: GLKView!
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setDiviceSupport()
        
        mySlider.setThumbImage(UIImage.init(named: "slider-cricle.png"), forState: .Normal)
        

        // save image at album
        //      TGCamera.setOption(kTGCameraOptionSaveImageToAlbum, value: NSNumber(bool: true))
        
        // hidden toggle button
        //TGCamera.setOption(kTGCameraOptionHiddenToggleButton, value: NSNumber(bool: true))
        //TGCameraColor.setTintColor(UIColor.greenColor())
        
        // hidden album button
        //      TGCamera.setOption(kTGCameraOptionHiddenAlbumButton, value: NSNumber(bool: true))
        
        // hide filter button
        //TGCamera.setOption(kTGCameraOptionHiddenFilterButton, value: NSNumber(bool: true))
        
        //self.settingCustomCamera()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpForGrayScaleCamera()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        session.stopRunning()
    }
    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(animated)
//        self.setUpForGrayScaleCamera()
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    //MARK: - **GrayScale Camara Methods**
    
    func setUpForGrayScaleCamera(){
        
        let sepiaColor = CIColor(
            red: 1.0 / 0.30078125,
            green: 1.0 / 0.5859375,
            blue: 1.0 / 0.11328125
        )
        
        filter = CIFilter(
            name: "CIColorMonochrome",
            withInputParameters: [
                "inputColor" : sepiaColor,
                "inputIntensity" : 1.0
            ]
        )
        

        
        // GL context
        
        let glContext = EAGLContext(
            API: .OpenGLES2
        )
        
        glView.context = glContext
        glView.enableSetNeedsDisplay = false
        
        context = CIContext(
            EAGLContext: glContext,
            options: [
                kCIContextOutputColorSpace: NSNull(),
                //kCIContextWorkingColorSpace: NSNull(),
            ]
        )
        
        let screenSize = UIScreen.mainScreen().bounds.size
        let screenScale = UIScreen.mainScreen().scale
        
        targetRect = CGRect(
            x: 0,
            y: 0,
            width: screenSize.width * screenScale,
            height: screenSize.height * screenScale
        )
        
        // Setup capture session.
        
        videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        let videoInput = try? AVCaptureDeviceInput(
            device: videoDevice
        )
        
        let videoQueue = dispatch_queue_create("video-queue", DISPATCH_QUEUE_SERIAL)
        
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: videoQueue)
        
        
        session = AVCaptureSession()
        session.beginConfiguration()
        session.addInput(videoInput)
        session.addOutput(videoOutput)
        session.commitConfiguration()
        
        
        stillImageOutput = AVCaptureStillImageOutput()
        let outputSettings:NSDictionary = [AVVideoCodecJPEG:AVVideoCodecKey]
        stillImageOutput?.outputSettings = outputSettings as [NSObject : AnyObject]
        session?.addOutput(stillImageOutput)
        
        
        session.startRunning()
        
        
        //For Zoom In and Zoom Out into the Camera
        let aSelector : Selector = #selector(self.handlePinchToZoomRecognizer(_:))
        let pinch:UIPinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action:aSelector )
        glView.addGestureRecognizer(pinch)
        
        //For Focus on Camera
        self.camaraTap()
        
        
        do{
            if (try(videoDevice?.lockForConfiguration()) != nil) {
                //mySlider?.setValue(-2.5, animated: false)
                videoDevice?.setExposureTargetBias(mySlider.value, completionHandler: nil)
                videoDevice?.unlockForConfiguration()
            }
            else{
                print("error")
            }
        }catch{
            print(error)
        }

    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let originalImage = CIImage(
            CVPixelBuffer: pixelBuffer,
            options: [
                kCIImageColorSpace: NSNull()
            ]
        )
        
        let rotatedImage: CIImage = originalImage.imageByApplyingTransform(rotationTransform)
        
        filter.setValue(rotatedImage, forKey: kCIInputImageKey)
        
        //let myFilteredImage : CIImage
        
        
        
        if (USERDEFAULT.valueForKey("on") != nil) {
            
            guard let grayFilteredImage = filter.outputImage else {
                return
            }
            
            let myOutputImage = self.myKernel().applyWithExtent(grayFilteredImage.extent, arguments: [grayFilteredImage])!
            
            guard let filteredImage = myOutputImage as CIImage!  else {
                return
            }
            
            //myFilteredImage = filteredImage as CIImage!
            
            context.drawImage(filteredImage, inRect: targetRect, fromRect: filteredImage.extent)
            
            if isCaptureImage {
                isCaptureImage = false
                SVProgressHUD.show()
                let myImage:UIImage = UIImage.init(CIImage: grayFilteredImage)
                self.cameraDidTakePhoto(myImage)
            }
        }
        else{
            guard let filteredImage = filter.outputImage else {
                return
            }
            
            //myFilteredImage = filteredImage as CIImage!
            
            context.drawImage(filteredImage, inRect: targetRect, fromRect: filteredImage.extent)
            
            if isCaptureImage {
                isCaptureImage = false
                SVProgressHUD.show()
                let myImage:UIImage = UIImage.init(CIImage: filteredImage)
                self.cameraDidTakePhoto(myImage)
            }
        }
        
//        context.drawImage(myFilteredImage, inRect: targetRect, fromRect: myFilteredImage.extent)
//        
//        if isCaptureImage {
//            isCaptureImage = false
//            SVProgressHUD.show()
//            let myImage:UIImage = UIImage.init(CIImage: myFilteredImage)
//            self.cameraDidTakePhoto(myImage)
//        }
        
        //glView.enableSetNeedsDisplay = true;
        //glView.setNeedsDisplay()
        glView.display()
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didDropSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        let seconds = CMTimeGetSeconds(CMSampleBufferGetPresentationTimeStamp(sampleBuffer))
        print("dropped sample buffer: \(seconds)")
    }
    
    //Add over saturation effect on image
    func myKernel() -> CIColorKernel{
        
        struct Static {
            static var once: dispatch_once_t = 0
            static var kernel: CIColorKernel? = nil
        }
        
        dispatch_once(&Static.once) {
            Static.kernel = CIColorKernel.init(string: "kernel vec4 CustomFilter ( __sample s ) \n { \n if ( s.r + s.g + s.b >= 1.0 ) \n { return s.rgba = vec4(1.0, 1.0, 0.0, 1.0); } \n else \n { return s.rgba; } \n }")!
        }
        
//        dispatch_once(&Static.once) {
//            Static.kernel = CIColorKernel.init(string: "kernel vec4 CustomFilter ( __sample s ) \n { \n if ( s.r + s.g + s.b < 0.1 ) \n { return s.rgba = vec4(1.0, 1.0, 0.0, 1.0); } \n else \n { return s.rgba; } \n }")!
//        }
        
        return Static.kernel!;
    }

    
    
    //MARK: - **Custom Camara Methods**
    func settingCustomCamera(){
        
        _captureSession = AVCaptureSession()
        
        //-- Creata a video device and input from that Device.  Add the input to the capture session.
        videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if videoDevice == nil {
            assert(true)
        }
        
        
        //-- Add the device to the session.
        do{
            let input:AVCaptureDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
           _captureSession?.addInput(input)
        }
        catch{
            print(error)
        }
        
     
        //-- Configure the preview layer
        _previewLayer = AVCaptureVideoPreviewLayer(session: _captureSession)
        _previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        _previewLayer?.frame = CGRectMake(0, 0, CamaraView.frame.size.width, CamaraView.frame.size.height)
        
        let previewLayerConnection:AVCaptureConnection = (_previewLayer?.connection)!
        
        if previewLayerConnection.supportsVideoOrientation {
            previewLayerConnection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight

        }
        
        //-- Add the layer to the view that should display the camera input
        CamaraView.layer.addSublayer(_previewLayer!)
        
        
        //-- Start the camera
        _captureSession?.startRunning()
        
        stillImageOutput = AVCaptureStillImageOutput()
        let outputSettings:NSDictionary = [AVVideoCodecJPEG:AVVideoCodecKey]
        stillImageOutput?.outputSettings = outputSettings as [NSObject : AnyObject]
        _captureSession?.addOutput(stillImageOutput)
      
//        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];

        let aSelector : Selector = #selector(self.handlePinchToZoomRecognizer(_:))
        let pinch:UIPinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action:aSelector )
        CamaraView.addGestureRecognizer(pinch)
        
        self.camaraTap()
        
        
        
    }
    
    func handlePinchToZoomRecognizer(pinchRecognizer:UIPinchGestureRecognizer){
        
        let pinchVelocityDividerFactor:CGFloat = 5.0
        if pinchRecognizer.state == .Changed {
            do{
                if (try(videoDevice?.lockForConfiguration()) != nil) {
                    if isnan(pinchRecognizer.velocity) {
                        return
                    }
                    
                    let desiredZoomFactor:CGFloat = (videoDevice?.videoZoomFactor)! + atan(pinchRecognizer.velocity / pinchVelocityDividerFactor)
                    videoDevice?.videoZoomFactor = max(1.0, min(desiredZoomFactor, (videoDevice?.activeFormat.videoMaxZoomFactor)!))
                    videoDevice?.unlockForConfiguration()
                }
                else{
                }

                
            }catch{
                print(error)
            }
                   }
        
    }
    
    
    @available(iOS 8.0, *)
    
    @IBAction func changeExposureTargetBias(sender:AnyObject){
        let control:UISlider = sender as! UISlider
        do{
            if (try(videoDevice?.lockForConfiguration()) != nil) {
                videoDevice?.setExposureTargetBias(control.value, completionHandler: nil)
                videoDevice?.unlockForConfiguration()
            }
            else{
                print("error")
            }
        }catch{
            print(error)
        }
        
        
    }
    
    func captureNow(){
        
        isCaptureImage = true
        
        return
        
//        SVProgressHUD.show()
//        var videoConnection:AVCaptureConnection?
//        for connection in stillImageOutput!.connections {
//            for port in connection.inputPorts {
//                if port.mediaType == AVMediaTypeVideo {
//                    videoConnection = connection as? AVCaptureConnection
//                    break
//                }
//            }
//            if (videoConnection != nil){
//                break
//            }
//        }
//
//        stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: { (imageSampleBuffer, error) in
//            
//            let imageData:NSData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageSampleBuffer)
//            var image:UIImage = UIImage(data: imageData)!
//            
//            image = UIImage(CGImage: image.CGImage!, scale: image.scale, orientation:.Up)
//
//            
//            self.cameraDidTakePhoto(image)
//        
//        })
    }
    
    func cameraDidTakePhoto(image:UIImage){
        
        var image:UIImage = image
        photoView.image = image
        
        //CGFloat scale = [[UIScreen mainScreen]scale];
        /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
        
        //UIGraphicsBeginImageContext(newSize);
        //UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
        //[image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        //UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        //UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContext(CGSizeMake(1138, 640))
        image.drawInRect(CGRectMake(0, 0, 1138, 640))
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
//        //Converting Image into Gray Scale
        //image = image.convertToGrayscale()
        
//        if (USERDEFAULT.valueForKey("on") != nil) {
//            image = image.convertToYellowscale()
//        }
//        else{
//            image = image.convertToGrayscale()
//        }
        
        //Converting Image into .jpeg format
        if (USERDEFAULT.valueForKey("jpeg") != nil) {
            let presentTimeStamp = self.getPresentDateTime()
            var fileSavePath = self.getDocumentsPath(presentTimeStamp)
            fileSavePath = fileSavePath.stringByAppendingString(".jpeg")
            
            if let data = UIImageJPEGRepresentation(image, 0.8) {
                data.writeToFile(fileSavePath as String, atomically: true)
            }
            
            
            let myToastString:NSString = "\(NSLocalizedString("Save file", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")) : \(presentTimeStamp.stringByAppendingString(".jpeg"))"
            
            print("My Image Name :  \(myToastString)")
            
            dispatch_async(dispatch_get_main_queue()) {
                APPDELEGATE.window!.makeToast(myToastString as String)
            }
            
            //APPDELEGATE.window!.makeToast(myToastString as String)
        }
            
            //Converting Image into .png format
//        else if(USERDEFAULT.valueForKey("png") != nil){
//            let presentTimeStamp = self.getPresentDateTime()
//            var fileSavePath = self.getDocumentsPath(presentTimeStamp)
//            fileSavePath = fileSavePath.stringByAppendingString(".png")
//         
//            if let data = UIImagePNGRepresentation(image) {
//                data.writeToFile(fileSavePath as String, atomically: true)
//            }
//
//            
//        }
        //else if(USERDEFAULT.valueForKey("tiff") != nil){
            
        //Converting Image into .tiff format
        else{
            
            self.convertImageIntoTIFF(image)
        }
        
        //For Saving Image in Gallery
//        UIImageWriteToSavedPhotosAlbum(image, self, #selector(HomeVC.image(_:didFinishSavingWithError:contextInfo:)), nil)
        
//        UIImageWriteToSavedPhotosAlbum(UIImage *image,
//                                       id completionTarget,
//                                       SEL completionSelector,
//                                       void *contextInfo);

        self.createAlbum(image)
        
        SVProgressHUD.dismiss()
        
        
        //let presentTimeStamp:NSString = "\(self.getPresentDateTime())"
        
        //APPDELEGATE.window!.makeToast(NSLocalizedString("Save file", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        guard error == nil else {
            //Error saving image
            return
        }
        //Image saved successfully
    }
    
    func camaraTap(){
        
        let shortTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapToFocus(_:)))
        shortTap.numberOfTapsRequired=1;
        shortTap.numberOfTouchesRequired=1;
        //CamaraView.addGestureRecognizer(shortTap)
        glView.addGestureRecognizer(shortTap)
    
    }
    
    func handleTapToFocus(tapGesture:UITapGestureRecognizer)  {
        
        if tapGesture.state == .Ended {
            
            let thisFocusPoint:CGPoint = tapGesture.locationInView(CamaraView)
            
            let focus_x = thisFocusPoint.x/CamaraView.frame.size.width
            let focus_y = thisFocusPoint.y/CamaraView.frame.size.height
            
            if videoDevice?.isFocusModeSupported(.AutoFocus) != nil && videoDevice?.focusPointOfInterestSupported != nil {
                do{
                
                if (try(videoDevice?.lockForConfiguration()) != nil) {
                    videoDevice?.focusMode = .AutoFocus
                    videoDevice?.focusPointOfInterest = CGPointMake(focus_x, focus_y)
                    videoDevice?.unlockForConfiguration()
                }
                    
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func setDiviceSupport()
    {
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            CamaraView.frame = CGRectMake(CamaraView.frame.origin.x, CamaraView.frame.origin.y, CamaraView.frame.size.width - 88 , CamaraView.frame.size.height)
            
            glView.frame = CGRectMake(glView.frame.origin.x, glView.frame.origin.y, glView.frame.size.width - 88 , glView.frame.size.height)
            
            photoView.frame = CGRectMake(photoView.frame.origin.x, photoView.frame.origin.y, photoView.frame.size.width-88, photoView.frame.size.height)
            
            mySlider.frame = CGRectMake(mySlider.frame.origin.x, mySlider.frame.origin.y, mySlider.frame.size.width-88, mySlider.frame.size.height)
        }
    }
    
    func convertImageIntoTIFF(myImage:UIImage) {
        
        var compression:Float = 1.0
        var orientation:Int = 1
        var myKeys = [CFStringRef]()
        var myValues = [CFTypeRef]()
        
        var myOptions:CFDictionaryRef!
        myKeys.append(kCGImagePropertyOrientation)
        myKeys.append(kCGImagePropertyHasAlpha)
        myKeys.append(kCGImageDestinationLossyCompressionQuality)
        
        myValues.append(CFNumberCreate(nil, .IntType, &orientation))
        myValues.append(kCFBooleanTrue)
        myValues.append(CFNumberCreate(nil, .FloatType, &compression))

        myOptions = CFDictionaryCreate(nil, UnsafeMutablePointer(myKeys), UnsafeMutablePointer(myValues), 3, nil, nil)
        
        let presentTimeStamp = self.getPresentDateTime()
        let aa  =   self.getDocumentsPath(presentTimeStamp)
        let filePath = aa.stringByAppendingString(".tiff")
        self.writeCGImage(myImage.CGImage!, url: NSURL.fileURLWithPath(filePath) , imageType: kUTTypeTIFF, options: myOptions)
        
        let myToastString:NSString = "\(NSLocalizedString("Save file", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")) : \(presentTimeStamp.stringByAppendingString(".tiff"))"
        
        print("My Image Name :  \(myToastString)")
        
        //APPDELEGATE.window!.makeToast(myToastString as String)
        dispatch_async(dispatch_get_main_queue()) {
            APPDELEGATE.window!.makeToast(myToastString as String)
        }
        
        let tiffUtility:TiffSetUtility = TiffSetUtility()
        tiffUtility.saveTiffGrey8Bit(filePath)
        //tiffUtility.setPhotometricTo(filePath)
        //tiffUtility.setBitPerSampleTo(filePath)
        //tiffUtility.setSamplePerPixelTo(filePath)
    }
    
//    func showToastView(){
//        print("Hiiii ")
//        
//        //APPDELEGATE.window!.makeToast(myToastString as String, duration: 1.0, position: CSToastPositionBottom)
//    }
    
    func writeCGImage(image:CGImageRef,url:NSURL,imageType:CFStringRef,options:CFDictionaryRef)
    {
        let myImageDest:CGImageDestinationRef = CGImageDestinationCreateWithURL(url, imageType, 1, nil)!
        CGImageDestinationAddImage(myImageDest, image, options)
        CGImageDestinationFinalize(myImageDest)
    }
    
    
    //MARK: - ACTION SHEET DELEGATE
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        
        switch buttonIndex
        {
            case 0:
                print("Cancel")
                break
            
            case 1:
                print("Gallery")
                imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.allowsEditing = true
                
                presentViewController(imagePicker, animated: true, completion: nil)
                break
            
            case 2:
                imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.allowsEditing = true
                
                if Platform.isSimulator {
                    imagePicker.sourceType = .PhotoLibrary
                }
                else {
                    imagePicker.sourceType = .Camera
                }
                
                presentViewController(imagePicker, animated: true, completion: nil)
                print("Camera")
                break
            
            default:
                dismissViewControllerAnimated(true, completion: nil)
                break
        }
    }
    
    //MARK: - IMAGEPICKER CONTROLLER DELEGATE
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var chosenImage:UIImage!
        
        if (USERDEFAULT.valueForKey("jpeg") != nil) {
            let presentTimeStamp = self.getPresentDateTime()
            var fileSavePath = self.getDocumentsPath(presentTimeStamp)
            fileSavePath = fileSavePath.stringByAppendingString(".jpeg")
            
            if (info[UIImagePickerControllerEditedImage] != nil) {
                if let data = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage, 0.8) {
                    data.writeToFile(fileSavePath as String, atomically: true)
                    chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
                }
                
            }
            else{
                if let data = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage] as! UIImage, 0.8) {
                    data.writeToFile(fileSavePath as String, atomically: true)
                    chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                }
            }

        }
        else if(USERDEFAULT.valueForKey("png") != nil){
            let presentTimeStamp = self.getPresentDateTime()
            var fileSavePath = self.getDocumentsPath(presentTimeStamp)
            fileSavePath = fileSavePath.stringByAppendingString(".png")
            
            if (info[UIImagePickerControllerEditedImage] != nil) {
                if let data = UIImagePNGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage) {
                    data.writeToFile(fileSavePath as String, atomically: true)
                    chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
                }
                
            }
            else{
                if let data = UIImagePNGRepresentation(info[UIImagePickerControllerOriginalImage] as! UIImage) {
                    data.writeToFile(fileSavePath as String, atomically: true)
                    chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                }
            }
        }
        
        photoView.image = chosenImage
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - OTHER METHODS
    
    func getPresentDateTime() -> NSString {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd_HH-mm-ss"
        let date = NSDate()
        
        let convertedDate = "PIE_\(dateFormatter.stringFromDate(date))"
        print("Date is \(convertedDate)")
        return convertedDate
    }
    
    
    func getDocumentsPath(fileName : NSString) -> NSString {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName as String)
        print(String(fileURL.path))
        return String(fileURL.path!)
        
    }
    
    func createAlbum(image:UIImage) {
        //Get PHFetch Options
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", AlbumName)
        
        let collection : PHFetchResult = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
        //Check return value - If found, then get the first album out
        if let _: AnyObject = collection.firstObject {
            self.albumFound = true
            
            assetCollection = collection.firstObject as! PHAssetCollection
            self.saveImage(image)
        } else {
            //If not found - Then create a new album
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({
                let createAlbumRequest : PHAssetCollectionChangeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollectionWithTitle(AlbumName)
                self.assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
                }, completionHandler: { success, error in
                    self.albumFound = (success ? true: false)
                    
                    if (success) {
                        let collectionFetchResult = PHAssetCollection.fetchAssetCollectionsWithLocalIdentifiers([self.assetCollectionPlaceholder.localIdentifier], options: nil)
                        print(collectionFetchResult)
                        self.assetCollection = collectionFetchResult.firstObject as! PHAssetCollection
                        
                        self.saveImage(image)
                    }
            })
        }
        
        
    }
    
    
    func saveImage(image:UIImage){
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            let assetRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(image)
            let assetPlaceholder = assetRequest.placeholderForCreatedAsset
            self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
            let albumChangeRequest = PHAssetCollectionChangeRequest(forAssetCollection: self.assetCollection, assets: self.photosAsset)
            albumChangeRequest!.addAssets([assetPlaceholder!])
            }, completionHandler: { success, error in
                print("added image to album")
                print(error)
        })
    }



  
    //MARK: - ACTION METHODS
    @IBAction func btnClicked(sender:UIButton){
        if (sender.tag == 10) {
            self.takePhotoTapped()
            
            

        }
        else if(sender.tag == 11){
            SVProgressHUD.show()
//            let ThumbnailVC = storyboard?.instantiateViewControllerWithIdentifier("ThumbnailVC")
//            self.navigationController?.pushViewController(ThumbnailVC!, animated: true)
            NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: #selector(self.pushToGalleryView), userInfo: nil, repeats: false)


            
        }
        else if(sender.tag == 12){
            let SettingVC = storyboard?.instantiateViewControllerWithIdentifier("SettingVC")
            self.navigationController?.pushViewController(SettingVC!, animated: true)

            
        }
        else if(sender.tag == 13){
            let app:UIApplication = UIApplication.sharedApplication()
            
            let aSelector : Selector = #selector(NSURLSessionTask.suspend)
            
            app.performSelector(aSelector)
            NSThread.sleepForTimeInterval(2.0)

        }
    }
    func pushToGalleryView() {
        
        let vc:UIViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("ThumbnailVC"))!
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func takePhotoTapped() {
        self.captureNow()
        
       // return
        
//        let actionSheet = UIActionSheet(title: "Choose your photo", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Gallery","Camera")
//        actionSheet.showInView(self.view)

    }
    
}
