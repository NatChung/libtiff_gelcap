//
//  ThumbnailCollectionVC.swift
//  GelCAP
//
//  Created by Pritesh Pethani on 20/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit

class ThumbnailCollectionVC: UICollectionViewCell {

    @IBOutlet var imageViewThumbnail:UIImageView!
    @IBOutlet var viewBorder:UIView!
    @IBOutlet var lblCount:UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setBorder()
        // Initialization code
    }
    
    func setBorder(){
        self.viewBorder.layer.borderWidth = 2;
        self.viewBorder.layer.borderColor = UIColor.whiteColor().CGColor;
        self.viewBorder.layer.cornerRadius = 10;
        self.viewBorder.layer.masksToBounds = true;
    }


}
