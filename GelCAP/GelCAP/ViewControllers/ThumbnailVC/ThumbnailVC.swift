//
//  ThumbnailVC.swift
//  GelCAP
//
//  Created by Pritesh Pethani on 20/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit

extension UIColor{
    
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: NSScanner = NSScanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersInString: "#")
        // Scan hex value
        scanner.scanHexInt(&hexInt)
        return hexInt
    }
}


class ThumbnailVC:UIViewController,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate{
    
    // MARK: - VARIABLE DECLARATION
    @IBOutlet var collectionViewImages:UICollectionView!
    @IBOutlet var textViewComment:UITextView!
    @IBOutlet var textViewFinalComment:UITextView!
    @IBOutlet var btnZoomClosed:UIButton!

    
    @IBOutlet var lblFileName:UILabel!
    @IBOutlet var lblNoData:UILabel!
    @IBOutlet var btnSave:UIButton!
    @IBOutlet var viewContainSaveButton:UIView!

    
    
    @IBOutlet var btnDelete:UIButton!
    @IBOutlet var btnComment:UIButton!

    //Custom View
    var viewImageZoom:UIView!
    var viewForTextView:UIView!

    @IBOutlet var viewImageContent:UIView!
    @IBOutlet var myScrollView:UIScrollView!

    @IBOutlet var viewComment:UIView!
    @IBOutlet var viewForTextViewBorder:UIView!
    
    @IBOutlet var lblHeader:UILabel!
    
    var isCellSelected:Bool!
    var myTag:Int!
    let reuseIdentifier = "myCell"

//    var cell:ThumbnailCollectionVC!
    
    
    var allTypeImages:NSMutableArray = NSMutableArray()
    var arrayOfImages:NSMutableArray = NSMutableArray()
    var zeroTempDataArray:NSMutableArray!
    var zeroDataArray:NSMutableArray!

    
    
    var fileName:String!
    var imageFullPathWithName:String!
    
    var minimumZoomScale:Float!
    var width:Float!
    var indexForZoomImage:Int!
    var currentPage:Int!
    


    // MARK: - VIEWCONTROLLER DELEGATE METHODS

    override func awakeFromNib() {
        super.awakeFromNib()
        zeroTempDataArray = NSMutableArray()
        zeroDataArray = NSMutableArray()

        viewImageZoom = NSBundle.mainBundle().loadNibNamed("ViewZoomImage", owner: self, options: nil)[0] as! UIView
        viewForTextView = NSBundle.mainBundle().loadNibNamed("textView", owner: self, options: nil)[0] as! UIView

        viewForTextView.frame = CGRectMake(0, 0, viewForTextView.frame.size.width, viewForTextView.frame.size.height)
        viewForTextView.hidden = true
        self.view.addSubview(viewForTextView)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.setLocalizationText()
        self.setDiviceSupport()
        
       // self.addTapGestureInOurView()
        self.enableDisableButton()
        self.borderSet()
       // self.setupLongTapGesture()
        
      
        self.performSelectorInBackground(#selector(self.viewControllerSetting), withObject: nil)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - OTHER METHODS
    func viewControllerSetting(){
        
        let nib = UINib(nibName: "ThumbnailCollectionVC", bundle: nil)
        collectionViewImages.registerNib(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        self.loadDocumentDirectoryImages()
   
    }
    
    func loadDocumentDirectoryImages(){
        let fileManager = NSFileManager.defaultManager()
        let stringPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        
        do{
            let filePathsArray = try fileManager.subpathsOfDirectoryAtPath(stringPath)
            
            for i in 0..<filePathsArray.count {
                print(filePathsArray[i])
                
                let strFilePath = filePathsArray[i]
                let imagePath = stringPath.stringByAppendingString("/").stringByAppendingString(strFilePath)
                
                //                let data:NSData = NSData(contentsOfFile: imagePath)!
                //
                //                let image:UIImage = UIImage(data: data)!
                
                let dict:NSMutableDictionary = [:]
                
                dict.setValue(strFilePath, forKey: "myImageName")
                dict.setValue(imagePath, forKey: "myImage")

                
                allTypeImages.addObject(dict)
                
            }
            
            arrayOfImages = allTypeImages.mutableCopy() as! NSMutableArray
            arrayOfImages = NSMutableArray(array: arrayOfImages.reverseObjectEnumerator().allObjects)
            
         //   arrayOfImages = (NSMutableArray )[[(NSArray )arrayOfImages reverseObjectEnumerator] allObjects];
            
            for _ in 0..<arrayOfImages.count {
                zeroTempDataArray.addObject("0")
                zeroDataArray.addObject("0")
            }
            
           // zeroDataArray.replaceObjectAtIndex(0, withObject: "1")

            
            
            if arrayOfImages.count == 0 {
                collectionViewImages.hidden = true
                lblNoData.hidden=false
            }
            else{
                collectionViewImages.hidden=false
                lblNoData.hidden=true
            }
            collectionViewImages.reloadData()
            
            self.setUpImagesIntoScrollView()
            
            print(allTypeImages)
            
        }catch{
            print(error)
        }
        
        
        SVProgressHUD.dismiss()
    }
    
    func borderSet(){
            viewComment.layer.borderWidth = 2;
            viewComment.layer.borderColor = UIColor.whiteColor().CGColor;
            viewComment.layer.cornerRadius = 10;
            viewComment.layer.masksToBounds = true;
            
            viewForTextViewBorder.layer.borderWidth = 2;
            viewForTextViewBorder.layer.borderColor = UIColor.whiteColor().CGColor;
            viewForTextViewBorder.layer.cornerRadius = 10;
            viewForTextViewBorder.layer.masksToBounds = true;
            
            
            
            textViewComment.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.5).CGColor
            textViewComment.layer.borderWidth = 1.0
            
            textViewComment.layer.cornerRadius = 5.0;
            textViewComment.clipsToBounds = true;
        
            textViewFinalComment.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.5).CGColor
            textViewFinalComment.layer.borderWidth = 1.0
        
            
            textViewFinalComment.layer.cornerRadius = 5.0;
            textViewFinalComment.clipsToBounds = true;
            
            
            
      
    }
    
    func addTapGestureInOurView() {
        let aSelector : Selector = #selector(ThumbnailVC.backgroundTap(_:))
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: aSelector)
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
   func backgroundTap(sender:UIGestureRecognizer){
        let point:CGPoint = sender.locationInView(sender.view)
        let viewTouched = view.hitTest(point, withEvent: nil)
        
        if viewTouched!.isKindOfClass(UIButton) && viewTouched!.isKindOfClass(UITextView) && viewTouched!.isKindOfClass(UICollectionViewCell){
            
        }
        else{
            self.view.endEditing(true)
        }
        
    }
    
    func enableDisableButton(){
        
        if self.isCellSelected != nil && self.isCellSelected! {
      
            btnSave.enabled = true;
            btnDelete.enabled = true;
            btnComment.enabled = true;
            textViewComment.userInteractionEnabled = true;
        }
        else{
            btnSave.enabled = false;
            btnDelete.enabled = false;
            btnComment.enabled = false;
            textViewComment.userInteractionEnabled = false;

        }
    }
    
    func setupLongTapGesture(){
        let bSelector : Selector = #selector(self.handleLongPress(_:))
        let longTap:UILongPressGestureRecognizer = UILongPressGestureRecognizer.init(target: self, action: bSelector)
        longTap.delaysTouchesBegan = true
        collectionViewImages.addGestureRecognizer(longTap)
        

    }
    
    func handleLongPress(gestureRecognizer:UILongPressGestureRecognizer){
        if gestureRecognizer.state != .Ended {
            return
        }
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .None)

        let point:CGPoint = gestureRecognizer.locationInView(collectionViewImages)
        
        let indexPath:NSIndexPath = collectionViewImages.indexPathForItemAtPoint(point)!
        
        indexForZoomImage = indexPath.row;
        
//        imageViewZoom.image = UIImage(contentsOfFile:arrayOfImages[indexPath.row].valueForKey("myImage") as! String )
        
        viewImageZoom.frame = CGRectMake(0, 0, viewImageZoom.frame.size.width, viewImageZoom.frame.size.height);
        
        myScrollView.contentOffset=CGPointMake(myScrollView.frame.size.width * CGFloat(indexForZoomImage), 0);

        self.view.addSubview(viewImageZoom)
        
        
    }
    
    func removeImage(filename:String){
        
        do{
            let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: path)
            let filePath = url.URLByAppendingPathComponent(filename).path!
            let fileManager = NSFileManager.defaultManager()
            if fileManager.fileExistsAtPath(filePath) {
                     try fileManager.removeItemAtPath(filePath)
            } else {
                
                print("FILE NOT AVAILABLE")
            }
            
        }catch{
            print(error)
        }
        
        if arrayOfImages.count > myTag {
            arrayOfImages .removeObjectAtIndex(myTag)
           zeroDataArray.removeObjectAtIndex(myTag)
            zeroTempDataArray.removeObjectAtIndex(myTag)
        }
        
        USERDEFAULT.removeObjectForKey(filename)
        USERDEFAULT.synchronize()
        lblFileName.text = NSLocalizedString("filename", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        textViewComment.text = ""
        self.isCellSelected = false;
        self.enableDisableButton()
        self.setUpImagesIntoScrollView()
        collectionViewImages.reloadData()

    }
    
    func setDiviceSupport(){
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            
            viewComment.frame = CGRectMake(viewComment.frame.origin.x, viewComment.frame.origin.y, viewComment.frame.size.width-88, viewComment.frame.size.height);
            textViewComment.frame = CGRectMake(textViewComment.frame.origin.x, textViewComment.frame.origin.y, textViewComment.frame.size.width-88, textViewComment.frame.size.height);
            btnDelete.frame = CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width + 25, btnDelete.frame.origin.y, btnDelete.frame.size.width, btnDelete.frame.size.height);
            
            lblFileName.frame = CGRectMake(lblFileName.frame.origin.x, lblFileName.frame.origin.y, lblFileName.frame.size.width-88, lblFileName.frame.size.height);
            lblFileName.font = lblFileName.font.fontWithSize(7.0)
            
            
            //TEXTVIEW
            viewForTextViewBorder.frame = CGRectMake(viewForTextViewBorder.frame.origin.x, viewForTextViewBorder.frame.origin.y, viewForTextViewBorder.frame.size.width-88, viewForTextViewBorder.frame.size.height);
            textViewFinalComment.frame = CGRectMake(textViewFinalComment.frame.origin.x, textViewFinalComment.frame.origin.y, textViewFinalComment.frame.size.width-88, textViewFinalComment.frame.size.height);
            viewContainSaveButton.frame = CGRectMake(textViewFinalComment.frame.origin.x+textViewFinalComment.frame.size.width + 10, viewContainSaveButton.frame.origin.y, viewContainSaveButton.frame.size.width, viewContainSaveButton.frame.size.height);
            
            //zoomView
            myScrollView.frame = CGRectMake(myScrollView.frame.origin.x, myScrollView.frame.origin.y, myScrollView.frame.size.width-88, myScrollView.frame.size.height);
            btnZoomClosed.frame = CGRectMake(myScrollView.frame.size.width-30, btnZoomClosed.frame.origin.y, btnZoomClosed.frame.size.width, btnZoomClosed.frame.size.height);
            

        }
        
    }
    
    
    func setLocalizationText(){
        textViewComment.placeholder="Comment"
        textViewComment.placeholderColor=UIColor.grayColor()
        textViewComment.font = UIFont.systemFontOfSize(14.0)
        
        
        textViewFinalComment.placeholder="Comment"
        textViewFinalComment.placeholderColor=UIColor.grayColor()
        textViewFinalComment.font = UIFont.systemFontOfSize(14.0);
        
        lblFileName.text = NSLocalizedString("filename", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeader.text = NSLocalizedString("Thumbnail", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoData.text = NSLocalizedString("No Thumbnail", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        textViewComment.placeholder = NSLocalizedString("Comment", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        textViewFinalComment.placeholder = NSLocalizedString("Comment", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        
    }
    
    func setUpImagesIntoScrollView(){
            if (arrayOfImages.count > 0) {
            
            var x:Int = 0
            myScrollView.pagingEnabled=true;
                for i:Int in 0..<arrayOfImages.count {
                    let myImageScrollView:UIScrollView = UIScrollView(frame: CGRectMake(CGFloat(x), 0,self.myScrollView.frame.size.width, self.myScrollView.frame.size.height))
                    
                    let imageViewZoom:UIImageView = UIImageView(frame: CGRectMake(0, 0,myImageScrollView.frame.size.width, myImageScrollView.frame.size.height))
                    
                    
                    
                    myImageScrollView.tag = i;
                    myImageScrollView.zoomScale = 1.0;
                    myImageScrollView.minimumZoomScale = 1.0;
                    myImageScrollView.maximumZoomScale = 3.0;
                    myImageScrollView.contentSize = imageViewZoom.frame.size;
                    myImageScrollView.delegate = self;
                    
                    imageViewZoom.image = UIImage(contentsOfFile: arrayOfImages[i].valueForKey("myImage") as! String)
                    x = x + Int(self.myScrollView.frame.size.width);
                    myImageScrollView.addSubview(imageViewZoom)
                    myScrollView.addSubview(myImageScrollView)

                }
            
            myScrollView.contentSize=CGSizeMake(CGFloat(x), myScrollView.frame.size.height);
            myScrollView.contentOffset=CGPointMake(0, 0);
            
            }
    
    }
    
    // MARK: - DELEGATE METHODS

    
    //MARK: - **CollectionView Delegate**
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfImages.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell:ThumbnailCollectionVC = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier,forIndexPath:indexPath) as! ThumbnailCollectionVC
        
        let url:NSURL = NSURL(fileURLWithPath: self.arrayOfImages[indexPath.row].valueForKey("myImage")as! String)
        
        
       // let url:NSURL = NSURL.init(fileURLWithPath: self.arrayOfImages[indexPath.row].valueForKey("myImage")as! String, isDirectory: true)
                
        cell.imageViewThumbnail.sd_setImageWithURL(url, placeholderImage: UIImage(named: "background"))
        
        //cell.imageViewThumbnail.sd_setImageWithURL(url, placeholderImage: UIImage(named: "background"), options: SDWebImageOptions.CacheMemoryOnly)
        
        cell.lblCount.text = String(indexPath.row+1)
        
        if zeroDataArray.objectAtIndex(indexPath.row).isEqualToString("1") {
            cell.viewBorder.layer.borderWidth = 2;
            cell.viewBorder.layer.borderColor = UIColor().HexToColor("#2C978C", alpha: 1.0).CGColor
            cell.viewBorder.layer.cornerRadius = 10;
            cell.viewBorder.layer.masksToBounds = true;
        }
        else{
            
            cell.viewBorder.layer.borderWidth = 2;
            cell.viewBorder.layer.borderColor = UIColor.whiteColor().CGColor
            cell.viewBorder.layer.cornerRadius = 10;
            cell.viewBorder.layer.masksToBounds = true;

        }
        
        let bSelector : Selector = #selector(self.handleLongPress(_:))
        let longTap:UILongPressGestureRecognizer = UILongPressGestureRecognizer.init(target: self, action: bSelector)
        longTap.delaysTouchesBegan = true
        cell.addGestureRecognizer(longTap)

        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.isCellSelected = true
      
        print("Zero Data Array \(zeroDataArray)")
        
        let myIndexValue = zeroDataArray.indexOfObject("1")

        
        zeroDataArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
       let oneDataArray:NSMutableArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        oneDataArray.replaceObjectAtIndex(indexPath.row, withObject: "1")
        
        
        zeroDataArray.replaceObjectAtIndex(indexPath.row, withObject: oneDataArray.objectAtIndex(indexPath.row))
        
        print("Zero Data Array \(zeroDataArray)")

        if (myIndexValue<zeroDataArray.count) {

            
        UIView.animateWithDuration(0) {
            self.collectionViewImages.performBatchUpdates({
                let indexPath2:NSIndexPath = NSIndexPath(forRow: myIndexValue, inSection: 0)
                self.collectionViewImages.reloadItemsAtIndexPaths([indexPath2])

                }, completion: nil)
        }
        
        }
        
//        let indexPath2:NSIndexPath = NSIndexPath(forRow: indexPath.row, inSection: 0)
//        collectionViewImages.reloadItemsAtIndexPaths([indexPath2])

        
        if (indexPath.row<zeroDataArray.count) {
          
            UIView.animateWithDuration(0) {
                self.collectionViewImages.performBatchUpdates({
                    let indexPath1:NSIndexPath = NSIndexPath(forRow: indexPath.row, inSection: 0)
                    self.collectionViewImages.reloadItemsAtIndexPaths([indexPath1])
                    
                    }, completion: nil)
            }
            
            
            
            myTag = indexPath.row
            
            lblFileName.text! = arrayOfImages[indexPath.row].valueForKey("myImageName") as! String
            
            let filename:NSString! = lblFileName.text
            
            textViewComment.text = USERDEFAULT.valueForKey(filename as String) as? String
            textViewFinalComment.text = textViewComment.text;
            
            fileName = filename as String
            imageFullPathWithName = arrayOfImages[indexPath.row].valueForKey("myImage") as! String
            
            self.enableDisableButton()

            
        }
        else{
           

        }


        
     
    }
    
//    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
//        
//      let cell:ThumbnailCollectionVC = collectionView.cellForItemAtIndexPath(indexPath) as! ThumbnailCollectionVC
//        
//            cell.viewBorder.layer.borderWidth = 2;
//            cell.viewBorder.layer.borderColor = UIColor.whiteColor().CGColor;
//            cell.viewBorder.layer.cornerRadius = 10;
//            cell.viewBorder.layer.masksToBounds = true;
//        
//        
//
//      
//    }

    
    
    // MARK: - **ScrollView Delegate**

    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        if (scrollView.tag == 1000) {
            return nil;
        }
        return scrollView.subviews[0];
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        minimumZoomScale = Float(scrollView.zoomScale)
        
        if (minimumZoomScale == 1.0) {
            myScrollView.scrollEnabled = true;
        }
        else{
            myScrollView.scrollEnabled = false;
        }

    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        
        width = Float(scrollView.frame.size.width)
        
        let xPos:Float = Float(scrollView.contentOffset.x)
        
        //print(xPos/width)
        
       currentPage = lround(Double(xPos)/Double(width));
//
//        //Calculate the page we are on based on x coordinate position and width of scroll view
//        NSLog(@" Current Page %ld",(long)currentPage);

    }
    
    
    // MARK: - **Textview Delegate**
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if (textView.tag == 10) {
            viewForTextView.hidden=false;
            UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .None)
            textViewFinalComment.performSelector(#selector(UIResponder.becomeFirstResponder), withObject: nil, afterDelay: 0.2)
        }
        else if(textView.tag == 11){
            
        }
        return true
    }


    // MARK: - **AlertView Delegate**
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int)
    {
        NSLog("Tag\(View.tag)")
        switch View.tag
        {
            case 1001:
                
                switch buttonIndex {
                    case 0:
                        //Remove Method Called
                        self.removeImage(lblFileName.text!)
                        break
                        
                    case 1:
                        break
                        
                    default:
                        break
                }
                break
                
            default:
                break
        }
        
    }
    
    func addUserComment(comment:String, path: String){
        
        if path.containsString("jpeg") {
            let container:ExifContainer = ExifContainer()
            container.addUserComment(comment)
            container.addDescription(comment)
            
            let image:UIImage = UIImage(contentsOfFile: path)!
            image.addExif(container).writeToFile(path, atomically: true)
            
        }else if path.containsString("tiff") {
            let tiffUtility:TiffSetUtility = TiffSetUtility()
            tiffUtility.setImageDescription(comment, to: path)
        }else {
            print("Do not match any type")
        }
    }
    
    // MARK: - ACTION METHODS
    @IBAction func btnTextViewDoneClicked(sender:AnyObject){
        
        let filename:NSString = lblFileName.text!
        
        USERDEFAULT.setValue(textViewFinalComment.text, forKey: filename as String)
        USERDEFAULT.synchronize()
        
        print(USERDEFAULT.valueForKey(filename as  String))
        
        textViewComment.text = textViewFinalComment.text;
        viewForTextView.hidden=true;
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .None)
        textViewFinalComment.resignFirstResponder()
        
        APPDELEGATE.window!.makeToast(NSLocalizedString("Comment Saved", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        addUserComment(textViewComment.text, path: imageFullPathWithName)
    }
    @IBAction func btnBackClicked(sender:AnyObject){
        dispatch_async(dispatch_get_main_queue()) {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func btnSaveClicked(sender:AnyObject){
        
        USERDEFAULT.setValue(textViewComment.text, forKey: lblFileName.text!)
        USERDEFAULT.synchronize()
        
        APPDELEGATE.window!.makeToast(NSLocalizedString("Comment Saved", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        
    }
    @IBAction func btnDeleteClicked(sender:AnyObject)
    {
        
        showAlertForDeleteImage(Appname, title: "\(NSLocalizedString("Delete file", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")): \(lblFileName.text!)", alertTag: 1001, withDelegate: self)
    }
    @IBAction func btnZoomViewCloseClicked(sender:AnyObject)
    {
        print("zeroTempDataArray \(zeroTempDataArray)")
        print("currentPage \(currentPage)")
        zeroDataArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        if currentPage == nil {
            currentPage = 0
        }        
        
        zeroDataArray.replaceObjectAtIndex(currentPage, withObject: "1")
        self.isCellSelected = true
        
        
        myTag = currentPage
        
        lblFileName.text! = arrayOfImages[currentPage].valueForKey("myImageName") as! String
        
        let filename:NSString! = lblFileName.text
        
        textViewComment.text = USERDEFAULT.valueForKey(filename as String) as? String
        textViewFinalComment.text = textViewComment.text;
        
        fileName = filename as String
        
        self.enableDisableButton()
        
        collectionViewImages.reloadData()
        
        
        
        

        viewImageZoom.removeFromSuperview()
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .None)
        
    }
    @IBAction func btnCommentClicked(sender:AnyObject)
    {
        viewForTextView.hidden=false;
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .None)
        textViewComment.resignFirstResponder()
        textViewFinalComment.becomeFirstResponder()
    }



}
