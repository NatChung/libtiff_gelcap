//
//  SetttingVC.swift
//  GelCAP
//
//  Created by Pritesh Pethani on 20/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit

class SetttingVC: UIViewController {

    // MARK: - VARIABLE DECLARATION
    @IBOutlet var btnJPEG:UIButton!
    @IBOutlet var btnPNG:UIButton!
    @IBOutlet var btnTIFF:UIButton!

    @IBOutlet var switchSaturation:UISwitch!
    @IBOutlet var lblHeader:UILabel!
    
    @IBOutlet var lblImageFormat:UILabel!
    @IBOutlet var lblOverSaturation:UILabel!
    @IBOutlet var lblJPEG:UILabel!
    @IBOutlet var lblPNG:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLocalizationText()
        self.viewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - OTHER METHODS
    
    func viewControllerSetting(){
        
        if (USERDEFAULT.valueForKey("jpeg") != nil) {
            self.radioBtnJPEGSelected(btnJPEG)
        }
        else if(USERDEFAULT.valueForKey("png") != nil){
            self.radioBtnPNGSelected(btnPNG)

        }
        else if(USERDEFAULT.valueForKey("tiff") != nil){
            self.radioBtnTIFFSelected(btnTIFF)
            
        }
        else{
            self.radioBtnJPEGSelected(btnJPEG)

        }
        
        if (USERDEFAULT.valueForKey("on") != nil) {
            switchSaturation.setOn(true, animated: true)
        }
        else if(USERDEFAULT.valueForKey("off") != nil){
            switchSaturation.setOn(false, animated: false)
            
        }
        else{
            switchSaturation.setOn(false, animated: false)
            
        }

        
   
    }
    
    func setFormatButton(radioButton:UIButton){
        
        let image:UIImage = UIImage(named: "Checkout_radio_Fill.png")!
        let image1:UIImage = UIImage(named: "Checkout_radio_Blank.png")!

        
        if(radioButton == btnJPEG)
        {
            
            btnJPEG.setImage(image, forState: .Normal)
            btnJPEG.adjustsImageWhenHighlighted = true;
            
            btnPNG.setImage(image1, forState: .Normal)
            btnTIFF.setImage(image1, forState: .Normal)

            
            btnJPEG.enabled = false;
            btnPNG.enabled = true;
            btnTIFF.enabled = true;

            
            USERDEFAULT.setValue("JPEG", forKey: "jpeg")
            USERDEFAULT.synchronize()
            
            USERDEFAULT.removeObjectForKey("png")
            USERDEFAULT.removeObjectForKey("tiff")

            
            
            NSLog("JPEG");
        }
        else if (radioButton == btnPNG)
        {
            btnPNG.setImage(image, forState: .Normal)
            btnPNG.adjustsImageWhenHighlighted = true;
            
            btnJPEG.setImage(image1, forState: .Normal)
            btnTIFF.setImage(image1, forState: .Normal)

            
            btnPNG.enabled = false;
            btnJPEG.enabled = true;
            btnTIFF.enabled = true;

            
            USERDEFAULT.setValue("PNG", forKey: "png")
            USERDEFAULT.synchronize()
            
            USERDEFAULT.removeObjectForKey("tiff")
            USERDEFAULT.removeObjectForKey("jpeg")
            
            NSLog("PNG");
            
        }
        else if (radioButton == btnTIFF)
        {
            btnTIFF.setImage(image, forState: .Normal)
            btnTIFF.adjustsImageWhenHighlighted = true;
            
            btnPNG.setImage(image1, forState: .Normal)
            btnJPEG.setImage(image1, forState: .Normal)
            
            
            btnTIFF.enabled = false;
            btnJPEG.enabled = true;
            btnPNG.enabled = true;
            
            
            USERDEFAULT.setValue("TIFF", forKey: "tiff")
            USERDEFAULT.synchronize()
            
            USERDEFAULT.removeObjectForKey("png")
            USERDEFAULT.removeObjectForKey("jpeg")
            
            NSLog("tiff");
            
        }
        
        
    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Settings", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblImageFormat.text = NSLocalizedString("Image Format :", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblOverSaturation.text = NSLocalizedString("Over Saturation :", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblJPEG.text = NSLocalizedString("JPEG", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblPNG.text = NSLocalizedString("PNG", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    

    

    // MARK: - ACTION METHODS
    @IBAction func btnBackClicked(sender:AnyObject){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func switched(sender:UISwitch){
        
        if sender.tag == 10 {
            if (sender.on) {
                USERDEFAULT.setValue("ON", forKey: "on")
                USERDEFAULT.synchronize()
                USERDEFAULT.removeObjectForKey("off")
            }
            else{
                USERDEFAULT.setValue("OFF", forKey: "off")
                USERDEFAULT.synchronize()
                USERDEFAULT.removeObjectForKey("on")
            }
        }
        
    }
    
    @IBAction func radioBtnJPEGSelected(sender:AnyObject){
        self.setFormatButton(btnJPEG)
    }
    @IBAction func radioBtnPNGSelected(sender:AnyObject){
        self.setFormatButton(btnPNG)
    }
    @IBAction func radioBtnTIFFSelected(sender:AnyObject){
        self.setFormatButton(btnTIFF)
    }
    
}
