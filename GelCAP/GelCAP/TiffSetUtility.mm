//
//  TiffSetUtility.m
//  GelCAP
//
//  Created by Nat on 2016/8/2.
//  Copyright © 2016年 Pritesh Pethani. All rights reserved.
//

#include "tiffio.h"
#import "TiffSetUtility.h"
#import "GreyscaleTiff.hpp"

extern "C" int command(int argc, char* argv[]);

@implementation TiffSetUtility

- (void)setImageDescription:(NSString *)description to:(NSString *)imagePath{
    char *argv[5] = {"tiffset", "-s", "ImageDescription", (char *)description.UTF8String, (char *)imagePath.UTF8String};
    command(5, argv);
}


- (void)saveTiffGrey8Bit:(NSString *)imagePath{
    
    Greyscale greyscale;
    
    greyscale.load_tiff(imagePath.UTF8String);
    greyscale.save_tiff_rgb(imagePath.UTF8String);
    greyscale.make_greyscale();
    greyscale.save_tiff_grey_8bit(imagePath.UTF8String);
    
}

@end
