//
//  GreyscaleTiff.cpp
//  GelCAP
//
//  Created by Nat on 2016/8/17.
//  Copyright © 2016年 Pritesh Pethani. All rights reserved.
//

#include "GreyscaleTiff.hpp"
#include "tiffio.h"


void Greyscale::load_tiff(std::string input_filename){
    m_image_data.clear();
    
    TIFF* tif = TIFFOpen(input_filename.c_str(), "r");
    if (tif) {
        uint32 w, h;
        size_t npixels;
        uint32* raster;
        
        TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
        TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
        npixels = w * h;
        m_width = w;
        m_height = h;
        
        raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32));
        if (raster != NULL) {
            if (TIFFReadRGBAImageOriented(tif, w, h, raster,ORIENTATION_TOPLEFT, 0)) {
                for(size_t n=0;n<npixels;n++) m_image_data.push_back(raster[n]);
            }
            _TIFFfree(raster);
        }
        TIFFClose(tif);
    }
}

void Greyscale::save_tiff_rgb(std::string output_filename) {
    TIFF *output_image;
    
    // Open the TIFF file
    if((output_image = TIFFOpen(output_filename.c_str(), "w")) == NULL){
        //cerr << "Unable to write tif file: " << output_filename << std::endl;
        printf("\n Unable to write tif file: %s", output_filename.c_str());
    }
    
    // We need to set some values for basic tags before we can add any data
    TIFFSetField(output_image, TIFFTAG_IMAGEWIDTH, m_width);
    TIFFSetField(output_image, TIFFTAG_IMAGELENGTH, m_height);
    TIFFSetField(output_image, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField(output_image, TIFFTAG_SAMPLESPERPIXEL, 4);
    TIFFSetField(output_image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    
    TIFFSetField(output_image, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE);
    TIFFSetField(output_image, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    
    // Write the information to the file
    TIFFWriteEncodedStrip(output_image, 0, &m_image_data[0], m_width*m_height * 4);
    
    // Close the file
    TIFFClose(output_image);
}

void Greyscale::save_tiff_grey_32bit(std::string output_filename) {
    TIFF *output_image;
    
    // Open the TIFF file
    if((output_image = TIFFOpen(output_filename.c_str(), "w")) == NULL){
        std::cerr << "Unable to write tif file: " << output_filename << std::endl;
    }
    
    // We need to set some values for basic tags before we can add any data
    TIFFSetField(output_image, TIFFTAG_IMAGEWIDTH, m_width);
    TIFFSetField(output_image, TIFFTAG_IMAGELENGTH, m_height);
    TIFFSetField(output_image, TIFFTAG_BITSPERSAMPLE, 32);
    TIFFSetField(output_image, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(output_image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    
    TIFFSetField(output_image, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE);
    TIFFSetField(output_image, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    
    // Write the information to the file
    TIFFWriteEncodedStrip(output_image, 0, &m_image_data[0], m_width*m_height * 4);
    
    // Close the file
    TIFFClose(output_image);
}

void Greyscale::save_tiff_grey_8bit(std::string output_filename) {
    TIFF *output_image;
    
    // Open the TIFF file
    if((output_image = TIFFOpen(output_filename.c_str(), "w")) == NULL){
        std::cerr << "Unable to write tif file: " << output_filename << std::endl;
    }
    
    // We need to set some values for basic tags before we can add any data
    TIFFSetField(output_image, TIFFTAG_IMAGEWIDTH, m_width);
    TIFFSetField(output_image, TIFFTAG_IMAGELENGTH, m_height);
    TIFFSetField(output_image, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField(output_image, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(output_image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    
    TIFFSetField(output_image, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(output_image, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE);
    
    // convert data to 8bit
    std::vector<uint8_t> data;
    for(size_t n=0;n<m_image_data.size();n++) {
        data.push_back(255-(m_image_data[n]/(256*256*256)));
    }
    
    // Write the information to the file
    TIFFWriteEncodedStrip(output_image, 0, &data[0], m_width*m_height);
    
    // Close the file
    TIFFClose(output_image);
}

void Greyscale::make_greyscale() {
    
    for(size_t n=0;n<m_image_data.size();n++) {
        
        double r = TIFFGetR(m_image_data[n]);
        double g = TIFFGetG(m_image_data[n]);
        double b = TIFFGetB(m_image_data[n]);
        
        double grey = (0.3*r) + (0.59*g) + (0.11*b); // See http://en.wikipedia.org/wiki/Grayscale
        m_image_data[n] = grey * 256 * 256 * 256;
    }
}
