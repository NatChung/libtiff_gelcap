//
//  UIImage+Grayscale.h
//
//  Created by Andreas Liebschner on 4/13/12.
//

#import <UIKit/UIKit.h>

@interface UIImage (Grayscale)

- (UIImage *)convertToYellowscale;
- (UIImage *)convertToGrayscale;

@end
