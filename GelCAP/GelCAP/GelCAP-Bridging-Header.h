//
//  GelCAP-Bridging-Header.h
//  GelCAP
//
//  Created by Pritesh Pethani on 14/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

#ifndef GelCAP_Bridging_Header_h
#define GelCAP_Bridging_Header_h


#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/UTCoreTypes.h>


#import "TGCameraViewController.h"
#import "TGCameraColor.h"
#import "UITextView+Placeholder.h"
#import "UIImagePickerController+OrientationFix.h"
#import "UIViewController+OrientationFix.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Grayscale.h"

#import "SVProgressHUD.h"
#import "TiffSetUtility.h"

#import <SimpleExif/ExifContainer.h>
#import <SimpleExif/UIImage+Exif.h>

#endif /* GelCAP_Bridging_Header_h */
