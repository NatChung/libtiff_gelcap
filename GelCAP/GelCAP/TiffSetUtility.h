//
//  TiffSetUtility.h
//  GelCAP
//
//  Created by Nat on 2016/8/2.
//  Copyright © 2016年 Pritesh Pethani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TiffSetUtility : NSObject
- (void)setImageDescription:(NSString *)description to:(NSString *)imagePath;
- (void)saveTiffGrey8Bit:(NSString *)imagePath;
@end
