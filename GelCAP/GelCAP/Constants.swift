//
//  Constants.swift
//  GelCAP
//
//  Created by Pritesh Pethani on 14/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import Foundation

//-----------APP DELEGATE ------------//

// MARK: - TableView Cell Identifiers

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64) && os(iOS)
            isSim = true
        #endif
        return isSim
    }()
}


let APPDELEGATE = UIApplication.sharedApplication().delegate as! AppDelegate
let USERDEFAULT = NSUserDefaults.standardUserDefaults()
let Appname = "GelCAP"

let AlbumName = "GelCAP"

func showAlertForDeleteImage(messageT:NSString,title:NSString,alertTag:Int,withDelegate:AnyObject) -> UIAlertView{
        let alert:UIAlertView = UIAlertView.init(title: messageT as String, message: title as String, delegate: nil, cancelButtonTitle: NSLocalizedString("Okay", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles: NSLocalizedString("Cancel", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        alert.show()
        alert.cancelButtonIndex = -1
        alert.delegate = withDelegate
        alert.tag = alertTag
        return alert
}



enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}
    



