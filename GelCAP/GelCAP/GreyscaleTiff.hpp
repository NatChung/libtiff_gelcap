//
//  GreyscaleTiff.hpp
//  GelCAP
//
//  Created by Nat on 2016/8/17.
//  Copyright © 2016年 Pritesh Pethani. All rights reserved.
//

#ifndef GreyscaleTiff_hpp
#define GreyscaleTiff_hpp

#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>

class Greyscale {
    std::vector<uint32_t> m_image_data;
    size_t m_width;
    size_t m_height;
    
public:
    Greyscale(){}
    void load_tiff(std::string input_filename) ;
    void save_tiff_rgb(std::string output_filename);
    void save_tiff_grey_32bit(std::string output_filename) ;
    void save_tiff_grey_8bit(std::string output_filename) ;
    void make_greyscale() ;
};

#endif /* GreyscaleTiff_hpp */
